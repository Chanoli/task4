package reflection;

public class Simple {
	public double length;
    public double width;
    private double height;

    public Simple() {
        this.length = 12.42;
        this.width = 6.42;
        this.height = 3.42;
    }
    public Simple(double length, double width, double height) {
        this.length = length;
        this.width = width;
        this.height = height;
    }
    public double calArea() {
        double area = length * width;
       return area;
    }
    private double calPerimeter(){
        double perimeter = 2 * (length + width);
        return perimeter;
    }
    private double calVolume(){
        double volume = 2 * (length + width * height);
        return volume;
    }
    public double getLength(){
        return length;
    }
    private void setLength(double length){
        this.length = length;
    }
    public double getWidth(){
        return width;
    }
    private void setWidth(double width){
        this.width = width;
    }
    public double getHeight(){
        return height;
    }
    public void setHeight(double height){
        this.height = height;
    }

    public String toString(){
      return "Length :" + length + ", Width : " + width + ", Height :" + height;
    }
}
