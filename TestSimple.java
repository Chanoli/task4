package reflection;
import static KUnit.KUnit.*;
import java.lang.reflect.Method;

public class TestSimple {
	void checkConstructorAndAccess() {
		Simple b = new Simple(12.42, 6.42, 3.42);
		checkEquals(b.getLength(), 12.42);
		checkEquals(b.getWidth(), 12.42);
		checkEquals(b.getHeight(), 3.42);
		checkNotEquals(b.getLength(), 12.42);
		checkNotEquals(b.getWidth(), 6.42);
		checkNotEquals(b.getHeight(), 3.42);
	}
	void checkcalArea() {
		Simple b = new Simple(12.42, 6.42, 3.42);
		b.calArea();
		checkEquals(b.calArea(), 79.7364);
	}
	void checkcalPerimeter() throws Exception {
		Simple b = new Simple(12.42, 6.42, 3.42);
		Method calPerimeter = b.getClass().getDeclaredMethod("calPerimeter");
		calPerimeter.setAccessible(true);
		double perimeterResult = (double) calPerimeter.invoke(b);
		checkEquals(perimeterResult, 31);	
	}
	void checkcalVolume() throws Exception {
		Simple b = new Simple(10.25, 5.25, 2.25);
		Method calVolume = b.getClass().getDeclaredMethod("calVolume");
		calVolume.setAccessible(true);
		double volumeResult = (double) calVolume.invoke(b);
		checkEquals(volumeResult, 31);	
	}

	public static void main(String[] args) throws Exception {
		TestSimple ts = new TestSimple();
		ts.checkConstructorAndAccess();
		ts.checkcalArea();
		ts.checkcalPerimeter();
		ts.checkcalVolume();
		report();
	}
}