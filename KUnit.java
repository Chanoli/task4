package KUnit;

import java.util.*;

public class KUnit {
	private static List<String> checks;
	private static int checkMade = 0;
	private static int passedChecks = 0;
	private static int failedChecks = 0;

	private static void addToReport(String txt) {
		if (checks == null) {
			checks = new LinkedList<String>();
		}
		checks.add(String.format("%04d: %s", checkMade++, txt));
	}

	public static void checkEquals(double value1, double value2 ) {
		if (value1 == value2 ) {
	        addToReport(String.format("%2f == %2f", value1, value2));
	        passedChecks++;
	    } else {
	        addToReport(String.format("%2f == %2f", value1, value2));
	        failedChecks++;
	    }
	}

	public static void checkNotEquals(double value1, double value2) {
		if (value1 != value2 ) {
	        addToReport(String.format("%2f == %2f", value1, value2));
	        passedChecks++;
	    } else {
	        addToReport(String.format("%2f == %2f", value1, value2));
	        failedChecks++;
	    }
	}

	public static void report() {
		System.out.printf("%d checks passed\n", passedChecks);
		System.out.printf("%d checks failed\n", failedChecks);
		System.out.println();
		for (String check : checks) {
			System.out.println(check);

		}
	}

}

