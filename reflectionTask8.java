package reflection;

import java.lang.reflect.Constructor;

public class reflectionTask8 {
	public static void main(String[] args) throws Exception {
        Simple b = new Simple();

        Constructor[] constructors = b.getClass().getConstructors();

        System.out.printf("There are %d public constructors\n", constructors.length);

        for (Constructor<?> c : constructors) {
            System.out.printf("Constructor name=%s", c.getName());
            System.out.println();
        }
    }
}
