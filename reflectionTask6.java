package reflection;

import java.lang.reflect.Method;

public class reflectionTask6 {
	public static void main(String[] args) throws Exception {
        Simple b = new Simple();
        Method m1 = b.getClass().getDeclaredMethod("setLength", double.class);
        Method m2 = b.getClass().getDeclaredMethod("setWidth", double.class);

        m1.setAccessible(true);
        m2.setAccessible(true);

        m1.invoke(b, 60.42);
        m2.invoke(b, 30.42);

        System.out.println(b);
    }
	
}
