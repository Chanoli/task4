package reflection;

import java.lang.reflect.Constructor;

public class reflectionTask7 {
	 public static void main(String[] args) throws Exception {
	        Simple b = new Simple();
	        Constructor[] constructors = b.getClass().getDeclaredConstructors();
	        System.out.printf("There are %d constructors\n", constructors.length);
	        for (Constructor c : constructors) {
	            System.out.printf("Constructor name=%s",
	                    c.getName());

	            System.out.println();
	        }

	    }
}