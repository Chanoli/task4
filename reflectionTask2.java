package reflection;

import java.lang.reflect.Field;

public class reflectionTask2 {
	public static void main(String[] args) throws Exception {
		Simple b = new Simple();
		Field[] fields = b.getClass().getFields();
		System.out.printf("There are %d fields\n", fields.length);
		for (Field f : fields) {
			System.out.printf("Field name=%s type=%s value=%2f\n", f.getName(), f.getType(), f.getDouble(b));
		}

	}
}
